package com.example.dialog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    TextView currentDateTime;
    Calendar dateAndTime=Calendar.getInstance();
    MyDateDialogFragment dateDialogFragment = new MyDateDialogFragment();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onProgressClick(View view)
    {
        MyProgressDialogFragment customProgress = MyProgressDialogFragment.getInstance();

// now you have the instance of CustomProgres
// for showing the ProgressBar
        Context context = null;
        customProgress.showProgress(context, "Loading",false);

// for hiding the ProgressBar

        customProgress.hideProgress();
    }

    public void onTimeClick(View view)
    {
        MyTimeDialogFragment ddf = MyTimeDialogFragment.newInstance(this, 0, dateAndTime);

        ddf.setTimeDialogFragmentListener(new MyTimeDialogFragment.TimeDialogFragmentListener() {

            @Override
            public void timeDialogFragmentDateSet(Calendar date) {
                // update the fragment
                TimePicker mDateDetailFragment = null;

            }
        });

        ddf.show(getSupportFragmentManager(), "date picker dialog fragment");
    }

    public void onDateClick(View view)
    {
        MyDateDialogFragment ddf = MyDateDialogFragment.newInstance(this, 0, dateAndTime);

        ddf.setDateDialogFragmentListener(new MyDateDialogFragment.DateDialogFragmentListener() {

            @Override
            public void dateDialogFragmentDateSet(Calendar date) {
                // update the fragment
                DatePicker mDateDetailFragment = null;
                mDateDetailFragment.updateDate(0,0,0);
            }
        });

        ddf.show(getSupportFragmentManager(), "date picker dialog fragment");
    }

    public void onClickShowDialog(View view) {
        MyDialogFragment dialogFragment = new MyDialogFragment();
        dialogFragment.show(getSupportFragmentManager(), "mirea");
    }
    public void onOkClicked() {
        Toast.makeText(getApplicationContext(), "Вы выбрали кнопку \"Иду дальше\"!",
                Toast.LENGTH_LONG).show();
    }

    public void onCancelClicked() {
        Toast.makeText(getApplicationContext(), "Вы выбрали кнопку \"Нет\"!",
                Toast.LENGTH_LONG).show();
    }
    public void onNeutralClicked() {
        Toast.makeText(getApplicationContext(), "Вы выбрали кнопку \"На паузе\"!",
                Toast.LENGTH_LONG).show();
    }
/*    public void setDate(View v) {
        new MyDateDialogFragment(MainActivity.this, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    // отображаем диалоговое окно для выбора времени
    public void setTime(View v) {
        new MyTimeDialogFragment(MainActivity.this, t,
                dateAndTime.get(Calendar.HOUR_OF_DAY),
                dateAndTime.get(Calendar.MINUTE), true)
                .show();
    }
    // установка начальных даты и времени
    private void setInitialDateTime() {

        currentDateTime.setText(DateUtils.formatDateTime(this,
                dateAndTime.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR
                        | DateUtils.FORMAT_SHOW_TIME));
    }

    // установка обработчика выбора времени
    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dateAndTime.set(Calendar.MINUTE, minute);
            setInitialDateTime();
        }
    };

    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime();
        }
    };*/

}
